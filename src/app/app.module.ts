import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import {ConfigService} from './config.service';

import { AppRoutingModule, routingComponent } from './app-routing.module';


import { HomeRestApiService } from './home/services/home-rest-api.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { RegistrationComponent } from './auth/registration/registration.component';

import { EmplyeeserviceService } from './auth/employeeservice';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';


@NgModule({
  declarations: [
    AppComponent,routingComponent, RegistrationComponent, LoginComponent, DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,NgxPaginationModule,
    HttpClientModule,
    FormsModule, ReactiveFormsModule
  ],
  providers: [HomeRestApiService, ConfigService, EmplyeeserviceService],
  bootstrap: [AppComponent]
})

export class AppModule { }
