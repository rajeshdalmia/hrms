import { TestBed } from '@angular/core/testing';

import { HomeRestApiService } from './home-rest-api.service';

describe('HomeRestApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HomeRestApiService = TestBed.get(HomeRestApiService);
    expect(service).toBeTruthy();
  });
});
