import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HomeInterFace } from '../module/homeInterface';

@Injectable({ 
  providedIn: 'root'
})
export class HomeRestApiService {

   apiUrl= `http://jsonplaceholder.typicode.com/users`;
  
  constructor(private _http : HttpClient) { }

   getHomecontent() {
     return this._http.get<HomeInterFace[]>(this.apiUrl)     
    }
  
}

