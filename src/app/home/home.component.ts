import { Component, OnInit } from '@angular/core';
// import { HomeRestApiService } from "./services/home-rest-api.service";
// import { HomeInterFace } from './module/homeInterface';
import { ConfigService } from '../config.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public home = {}

  constructor( private config : ConfigService) { }

  ngOnInit() {
     this.home = this.getHomeItem();
  }
  getHomeItem (){
    return this.config.getConfig().home;
  }
  // user$ : HomeInterFace[];
  
  //  constructor (public HomApiServices : HomeRestApiService ){}

  // ngOnInit() {    
  //   return this.HomApiServices.getHomecontent()
  //   .subscribe(data => this.user$ = data)
  // }
}