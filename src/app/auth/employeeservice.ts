import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Employee} from './employee';
import {Loginemployee} from './login/loginemployee';

@Injectable({
  providedIn: 'root'
})
export class EmplyeeserviceService {
url='http://localhost/edu/public/api/';

  constructor(private http:HttpClient) { }
 
  createemployee(employee:Employee):Observable<Employee>{
    return this.http.post<Employee>(this.url+'user',employee)
  }

  loginemployee(loginEmployee:Loginemployee):Observable<Employee> {
    return this.http.post<Employee>(this.url + 'login', loginEmployee)
  }

}