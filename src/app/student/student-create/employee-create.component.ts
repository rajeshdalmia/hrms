import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../rest-api.service';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})

export class EmployeeCreateComponent implements OnInit {

  @Input() employeeDetails = { 
    title: '', description: '', meta_title: '', meta_description : '',
    cstatus : '', created_at : '',  updated_at : ''
 }
  
   constructor(public restApi: RestApiService, public router: Router) { }

  ngOnInit() { }

  addEmployee(dataEmployee) {
    this.restApi.createEmployee(this.employeeDetails).subscribe((data: {}) => {
      this.router.navigate(['/student-list'])
    })
  }

}