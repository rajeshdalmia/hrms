export class Employee {
    id: string;
    title: string;
    description: string;
    meta_title: string;
    meta_description : string;
    cstatus : string;
    created_at : string;
    updated_at : string;
 }