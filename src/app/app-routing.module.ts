import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { OurVerticalsComponent } from './our-verticals/our-verticals.component';
import { OurPromoterComponent } from './our-promoter/our-promoter.component';
import { CareerComponent } from './career/career.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { RegistrationComponent } from './auth/registration/registration.component';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';

import { EmployeeCreateComponent } from './student/student-create/employee-create.component';
import { EmployeeEditComponent } from './student/student-edit/employee-edit.component';
import { EmployeeListComponent } from './student/student-list/employee-list.component';
import { TestiomialComponent } from './aboutus/testiomial/testiomial.component';
import { PreloadAllModules } from '@angular/router';

const routes: Routes = [
 
  { path: '', redirectTo: '/Home', pathMatch: 'full' },
  { path: 'Home', component: HomeComponent },

   { path: 'AboutUs', component: AboutusComponent },
  
  //  {path : 'AboutUs', loadChildren :'./aboutus/aboutus.component#AboutusModule'},

  { path: 'OurVerticals', component: OurVerticalsComponent },
  { path: 'OurPromoter', component: OurPromoterComponent },
  { path: 'Career', component: CareerComponent },
  { path: 'ContactUs', component: ContactUsComponent },
  { path: 'register', component: RegistrationComponent },
  { path: 'login', component: LoginComponent },
  {path:'dashboard',component:DashboardComponent},
  { path: 'create-student', component: EmployeeCreateComponent},
  { path: 'student-list', component: EmployeeListComponent },
  { path: 'student-edit/:id', component: EmployeeEditComponent, data:{title : 'edit employee detail'} },
  
  // {path: 'testiomial', 
  //    loadChildren: 'app/aboutus/testiomial/testiomial.component#AboutusModule'
  // },

];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {
      preloadingStrategy: PreloadAllModules
    }) 
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }

export const routingComponent = [
  HeaderComponent, FooterComponent, HomeComponent,
  AboutusComponent,OurVerticalsComponent, OurPromoterComponent,
  CareerComponent, TestiomialComponent,
  EmployeeListComponent, EmployeeEditComponent,EmployeeCreateComponent,
  ContactUsComponent
]