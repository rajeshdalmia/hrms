import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestiomialComponent } from './testiomial.component';

describe('TestiomialComponent', () => {
  let component: TestiomialComponent;
  let fixture: ComponentFixture<TestiomialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestiomialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestiomialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
