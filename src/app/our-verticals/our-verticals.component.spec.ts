import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurVerticalsComponent } from './our-verticals.component';

describe('OurVerticalsComponent', () => {
  let component: OurVerticalsComponent;
  let fixture: ComponentFixture<OurVerticalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurVerticalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurVerticalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
