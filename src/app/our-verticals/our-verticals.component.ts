import { Component, OnInit } from '@angular/core';
import { ServicesService } from './services.service';
import { VerticalInterface } from './model/vertical-interface';


@Component({
  selector: 'app-our-verticals',
  templateUrl: './our-verticals.component.html',
  styleUrls: ['./our-verticals.component.css']
})
export class OurVerticalsComponent implements OnInit {

  user1 : VerticalInterface[];

  constructor(public verticleService : ServicesService) { }

  ngOnInit() {
    return this.verticleService.getVerticleData()
    .subscribe(data => this.user1 = data)
  }

}
