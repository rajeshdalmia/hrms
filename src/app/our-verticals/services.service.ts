import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {VerticalInterface} from './model/vertical-interface';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
 
  apiUrl1= `http://jsonplaceholder.typicode.com/posts`;

  constructor( private _http : HttpClient) { }

  getVerticleData(){
    return this._http.get<VerticalInterface[]>(this.apiUrl1)
  }

 
}
