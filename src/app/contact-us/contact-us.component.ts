import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  category : Array<String> = ['Category1','Category2', 'Category3'];


  constructor(private fb : FormBuilder) { }

  contactForm = this.fb.group({
    name: [''],
    email : [''],
    mobile : [''],
    category : [''],
    message : ['']
  });

  ngOnInit() {
  }

}
