import { Component, OnInit } from '@angular/core';
import { EmplyeeserviceService } from 'src/app/auth/employeeservice';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    console.log(JSON.parse(localStorage.getItem('UserId')));
  }

  logout(){
    localStorage.removeItem('UserId');
    
  }

}
