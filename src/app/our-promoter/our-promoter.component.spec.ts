import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OurPromoterComponent } from './our-promoter.component';

describe('OurPromoterComponent', () => {
  let component: OurPromoterComponent;
  let fixture: ComponentFixture<OurPromoterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OurPromoterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OurPromoterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
